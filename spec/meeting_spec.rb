require 'spec_helper'

describe Meeting do
  subject(:tps_api) { Meeting }
  it 'returns false if no conference id specified' do
    expect { tps_api.delete_meeting({}) }.to raise_error
  end
end
