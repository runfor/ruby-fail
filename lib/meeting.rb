require 'nokogiri'
require 'bundler'
require 'erubis'
require 'faraday'
require 'faraday_middleware'
require 'em-http'
require 'typhoeus'
require 'typhoeus/adapters/faraday'
require 'socket'

module Meeting
  def self.delete_meeting(meeting)
    raise 'Conference must a valid conference id' unless conference[:id].to_i > 0
  end
end
